import argparse
import sys
from datetime import datetime
from pathlib import Path


def build_version(major, minor, patch, branch, build, meta):
    dot = "."
    return f'{major}.{minor}.{patch}-{branch}.{build}' \
           f'{f"+{dot.join(meta)}" if meta else ""}'


def get_current_version(major=None, branch=None, meta=None, current=None):

    if major is None:
        major = '0'

    if branch is None:
        branch = 'local'

    if current is None:
        current = datetime.utcnow()

    return build_version(major, current.strftime("%Y%m"), current.day, branch, current.strftime("%H%M"), meta)


def parse_version(file_path):
    with open(Path(file_path)) as f:
        verstr = f.readline()
        base, extra = verstr.split('-')
        major, minor, patch = base.split('.')
        extra_base, *meta = extra.split('+')
        branch, build = extra.split('.', 1)
        return major, minor, patch, branch, build, meta


def update_version(file_path, major=None, branch=None, meta=None, current=None):
    parsed_major, parsed_minor, parsed_patch, parsed_branch, parsed_build, parsed_meta = parse_version(file_path)

    major = major or parsed_major
    branch = branch or parsed_branch

    with open(Path(file_path), 'w') as f:
        print(get_current_version(major, branch, meta, current), file=f)


def cli():
    arg_parser = argparse.ArgumentParser(description='Remak8s Version Manager')
    arg_parser.add_argument('file', nargs='?', default=None, help='Path to the version file to manage')
    arg_parser.add_argument('--update', default=False, action='store_true', help='Update the file')
    # arg_parser.add_argument('--current', default=False, action='store_true', help='Output the current version')

    arg_parser.add_argument('--major', default='0', help='Override major version')
    arg_parser.add_argument('--branch', default=None, help='Override branch')
    arg_parser.add_argument('--meta', default=None, help='Override meta')

    args = arg_parser.parse_args(sys.argv[1:])

    if args.update and args.file:
        update_version(args.file)
    elif args.file:
        parsed_major, parsed_minor, parsed_patch, parsed_branch, parsed_build, parsed_meta = parse_version(args.file)
        major = args.major or parsed_major
        branch = args.branch or parsed_branch
        meta = args.meta or parsed_meta
        print(build_version(major, parsed_minor, parsed_patch, branch, parsed_build, meta))
    else:
        print(get_current_version(args.major, args.branch, args.meta))


if __name__ == '__main__':
    cli()
