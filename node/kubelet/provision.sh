#!/usr/bin/env bash

K8S_VERSION="v1.17.0"

# TODO: add ability to join existing cluster instead of only provisioning master node

CP_ENDPOINT=$(cat /etc/hostname)
PN_CIDR="192.168.0.0/16"

if test -f "/etc/kubernetes/kubelet.conf"; then
    echo "Kubelet already provisioned"
    exit 0
fi

echo "Provisioning kubelet (master node)..."
echo "Using cluster control plane endpoint: $CP_ENDPOINT"
echo "Using k8s version: $K8S_VERSION"

cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

echo "Installing kube..."
dnf install -y kubelet kubectl kubeadm --disableexcludes=kubernetes
echo "Kube installed"

systemctl enable --now kubelet

kubeadm init \
  --kubernetes-version="$K8S_VERSION" \
  --control-plane-endpoint="$CP_ENDPOINT" \
  --pod-network-cidr="$PN_CIDR" || exit 1

mkdir -p /root/.kube
sudo cp -i /etc/kubernetes/admin.conf /root/.kube/config

kubectl apply -f https://docs.projectcalico.org/v3.8/manifests/calico.yaml

kubectl taint nodes --all node-role.kubernetes.io/master-

#echo "Patching coredns"

#COREFILE="
#    .:53 {
#      health {
#        lameduck 5s
#      }
#      ready
#
#      kubernetes cluster.local in-addr.arpa ip6.arpa {
#        pods insecure
#        fallthrough in-addr.arpa ip6.arpa
#          ttl 30
#      }
#
#      prometheus :9153
#      forward . /etc/resolv.conf
#      cache 30
#      loop
#      reload
#      loadbalance
#    }
#"

#kubectl patch configmap/coredns \
#  -n kube-system \
#  --type merge \
#  -p "{\"data\":{\"Corefile\":\"$COREFILE\"}}"

echo "Installling helm 3..."

curl -q -L https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash || true
ln -s /usr/local/bin/helm /usr/bin || true

helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo update

echo "Provisioning kubelet... complete!"