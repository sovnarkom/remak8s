#!/usr/bin/env bash

DIR=/remak8s/node/ingress

EXTERNAL_IP=$1
DOMAIN=$(cat /etc/hostname)

echo "Provisioning traefik ingress controller..."

helm repo update

kubectl apply -f "$DIR/traefik.persistent-volume.yml"
helm upgrade --install traefik \
  --namespace kube-system \
  --values "$DIR/traefik.values.helm.yml" \
  --set externalIP="$EXTERNAL_IP" \
  --set dashboard.domain="ingress.$DOMAIN" \
  stable/traefik

echo "Provisioning traefik ingress controller complete."
