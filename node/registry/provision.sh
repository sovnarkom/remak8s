#!/usr/bin/env bash

echo "Provisioning docker registry..."

DIR=/remak8s/node/registry
NAPESPACE="registry"
DOMAIN=$(cat /etc/hostname)

kubectl delete namespace $NAPESPACE
kubectl create namespace $NAPESPACE

helm upgrade --install registry stable/docker-registry -f "$DIR/values.helm.yml" \
  --namespace=$NAPESPACE \
  --set=ingress.hosts[0]="registry.$DOMAIN"

# TODO: add this to containerd   /etc/containerd/config.toml and reload
# https://registry.remak8s.k8s/
#        [plugins.cri.registry.mirrors."registry.remak8s.k8s"]
#          endpoint = ["http://registry.remak8s.k8s"]
#
# When containerd 1.3+ available
#        [plugins.cri.registry.configs."registry.remak8s.k8s".tls]
#          insecure_skip_verify = true
#


echo "Provisioning docker registry... complete!"
