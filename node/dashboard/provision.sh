#!/usr/bin/env bash

DIR=/remak8s/node/dashboard
VERSION="v2.0.0-beta8"

# TODO: make this configurable
SECURE=0

echo "Provisioning k8s dashboard ($VERSION)..."

kubectl delete namespace kubernetes-dashboard
kubectl create namespace kubernetes-dashboard

kubectl apply -f "$DIR/service-account.yaml"

if test $SECURE == 1; then
  echo "Applying secure role binding"
  kubectl delete -f "$DIR/insecure.cluster-role-binding.yaml"
  kubectl apply -f "https://raw.githubusercontent.com/kubernetes/dashboard/$VERSION/aio/deploy/recommended.yaml"
  kubectl apply -f "$DIR/cluster-role-binding.yaml"
  kubectl apply -f "$DIR/ingress.yaml"
else
  echo "Applying insecure role binding"
  kubectl delete clusterrolebinding kubernetes-dashboard
  kubectl apply -f "https://raw.githubusercontent.com/kubernetes/dashboard/$VERSION/aio/deploy/alternative.yaml"
  kubectl delete clusterrolebinding kubernetes-dashboard
  kubectl patch deployment kubernetes-dashboard --patch "$(cat $DIR/insecure.deployment.patch.yaml)" -n kubernetes-dashboard -o yaml
  kubectl apply -f "$DIR/insecure.cluster-role-binding.yaml"
  kubectl apply -f "$DIR/insecure.ingress.yaml"
fi

echo "Provisioning k8s dashboard complete."

#kubernetes-dashboard-csrf