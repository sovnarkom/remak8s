#!/usr/bin/env bash

TITLE="Provisioning NATS Operator"

echo "$TITLE..."

DIR=/remak8s/node/nats
NAPESPACE="nats"

kubectl delete namespace $NAPESPACE
kubectl create namespace $NAPESPACE

gryml $DIR/nats-operator/ --set namespace=$NAPESPACE --echo | kubectl apply -f -
gryml $DIR/nats-streaming-operator/ --set namespace=$NAPESPACE --echo | kubectl apply -f -
gryml $DIR/default-cluster/ --set namespace=$NAPESPACE --set name=default --echo | kubectl apply -f -

echo "$TITLE complete!"
