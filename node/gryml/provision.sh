#!/usr/bin/env bash

DIR=/remak8s/node/gryml
VERSION="local"

echo "Provisioning gryml ($VERSION)..."

python3 -m venv /venv/gryml
/venv/gryml/bin/pip install --upgrade pip
/venv/gryml/bin/pip install -r /remak8s/gryml/requirements.txt

ln -s $DIR/gryml.sh /usr/bin/gryml
chmod +x /usr/bin/gryml

echo "Provisioning gryml ($VERSION) complete."
