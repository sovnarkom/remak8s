#!/usr/bin/env bash

DIR=/remak8s/node/tekton
VERSION="latest"

CLI_VERSION="0.6.0"
DASHBOARD_VERSION="0.3.0"

echo "Provisioning tekton ($VERSION)..."

echo "Applying CRDs..."
kubectl apply --filename "https://storage.googleapis.com/tekton-releases/pipeline/latest/release.yaml"

if [ ! -f  /usr/local/bin/tkn ]; then
  echo "Installing tkn cli..."
  curl -LO "https://github.com/tektoncd/cli/releases/download/v${CLI_VERSION}/tkn_${CLI_VERSION}_Linux_x86_64.tar.gz"
  tar xvzf tkn_${CLI_VERSION}_Linux_x86_64.tar.gz -C /usr/local/bin/ tkn
fi

echo "Applying tkn dashboard..."
kubectl apply --filename https://github.com/tektoncd/dashboard/releases/download/v${DASHBOARD_VERSION}/dashboard-latest-release.yaml
kubectl apply -f "$DIR/ingress.yaml"


mkdir /var/kaniko-cache/
chown vagrant:vagrant /var/kaniko-cache/

echo "Provisioning tekton $(/usr/local/bin/tkn version) complete."
