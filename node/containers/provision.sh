#!/usr/bin/env bash

if test -d "/var/run/containerd/"; then
    echo "Containerd already provisioned"
    exit 0
fi

echo "Provisioning conainers dependencies... "

echo "Using containerd."

cat > /etc/modules-load.d/containerd.conf <<EOF
overlay
br_netfilter
EOF

modprobe overlay
modprobe br_netfilter

cat > /etc/sysctl.d/99-kubernetes-cri.conf <<EOF
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF

sysctl --system

dnf install -y device-mapper-persistent-data lvm2

dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo

dnf install -y containerd.io

mkdir -p /etc/containerd
containerd config default > /etc/containerd/config.toml

systemctl enable --now containerd

echo "Provisioning conainers dependencies... complete!"
