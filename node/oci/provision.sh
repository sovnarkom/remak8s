#!/usr/bin/env bash

echo "Provisioning OCI image builder (buildah)..."

# TODO: we should probably replace this with Kaniko in K8S though

DIR=/remak8s/node/oci

curl -LO "http://download-ib01.fedoraproject.org/pub/fedora/linux/releases/29/Everything/x86_64/os/Packages/b/buildah-1.4-1.dev.git0a7389c.fc29.x86_64.rpm"
dnf install -y buildah-1.4-1.dev.git0a7389c.fc29.x86_64.rpm

# FIXME - having some regression in the current verin
#dnf install -y buildah

echo "Provisioning OCI image builder (buildah)... complete!"
