#!/usr/bin/env bash

# TODO: this is not strictly necessary, as kopf can deploy the operator
# But we want to eventually be able to build and deploy the image to the public docker hub
# buildah bud /remak8s/operator/ -t registry.remak8s.k8s/remak8s:latest

pip3 install kopf

python3 -m venv /venv/remak8s
/venv/remak8s/bin/pip install --upgrade pip
/venv/remak8s/bin/pip install -r /remak8s/operator/requirements.txt

# TODO: actually deploy the operator
# https://kopf.readthedocs.io/en/latest/deployment/