# Node provisioning scripts

Theese scripts wrap `kubeadm` and some other tools to simplify the setup of the simple single-node k8s cluster from
scratch but also can be used to install remak8s on the existing cluster.

Also used by the vagrant environment, embedded into remak8s for provisioning.
 

