#!/usr/bin/env bash

LOCAL_NETWORK=$1
LOCAL_HOST=$(cat /etc/hostname)

echo "Provisioning base dependencies... "

setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

if grep -qe "^/swapfile" /etc/fstab; then
  sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
  swapoff -a
fi

sed "s/.*k8s.*/$LOCAL_NETWORK $LOCAL_HOST # k8s master\n$LOCAL_NETWORK registry.$LOCAL_HOST # k8s registry\n/" -i /etc/hosts

dnf -y install dnf-plugins-core nano git

update-alternatives --set iptables /usr/sbin/iptables-legacy

echo "Provisioning base dependencies... complete!"