#!/usr/bin/env bash

echo "Provisioning nfs server persistent volume provisioner..."

DIR=/remak8s/node/volumes
NAPESPACE="nfs"

# TODO: handle re-provision
# kubectl get pv | tail -n+2 | awk '{print $1}' | xargs -I{} kubectl patch pv {} -p '{"metadata":{"finalizers": null}}'
# kubectl delete namespace $NAPESPACE

dnf install -y nfs-utils

kubectl create namespace $NAPESPACE

kubectl apply -f "$DIR/local.storage-class.yml"
kubectl apply -f "$DIR/nfs.persistent-volume.yml" --namespace=$NAPESPACE

helm upgrade --install nfs stable/nfs-server-provisioner -f "$DIR/nfs.values.helm.yml" --namespace=$NAPESPACE

echo "Provisioning nfs server persistent volume provisioner... complete!"
