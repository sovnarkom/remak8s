#!/usr/bin/env bash

TITLE="Provisioning Prometheus Operator"

echo "$TITLE..."

DIR=/remak8s/node/prometheus
NAPESPACE="prometheus"
DOMAIN=$(cat /etc/hostname)

kubectl delete namespace $NAPESPACE
kubectl create namespace $NAPESPACE

helm upgrade --install operator stable/prometheus-operator -f "$DIR/values.helm.yml" \
  --namespace $NAPESPACE

# TODO: move to values
helm upgrade --install prom-adapter stable/prometheus-adapter \
  --set prometheus.url="http://operator" \
  --set prometheus.port="9090" \
  --set image.tag="v0.4.1" \
  --set rbac.create="true" \
  --namespace $NAPESPACE \


echo "$TITLE complete!"
