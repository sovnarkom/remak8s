# Vagrant Tools

This part of remak8s allows us to quickly setup local development and testing environment 
with proper single node k8s cluster. 

It can build, deploy and debug both remak8s itself and pipelines managed by remak8s and can be used to setup your
own local development SDK for pretty much any project.


## Disclamer 

If you hate Vagrant and VirtualBox - we understand, we have love/hate relationship with it for a lot of years.
But, proper virtual machine is still more reliable if your internet is unreliable while RAM is cheap.

We'd love to replace Vagrant with something modern as soon as we find something capable of managing hosts, custom
networks, base images supporting k8s as natively as possible, having cli to automate quick reprovisioning and teardown. 

You can, of course, use this SDK in a remote cluster as long as you have necessary access.
