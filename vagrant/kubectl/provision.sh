#!/usr/bin/env bash

echo "Provisioning vagrant specific kubeadm settings..."

mkdir -p /home/vagrant/.kube
cp -i /etc/kubernetes/admin.conf /home/vagrant/.kube/config
chown -R vagrant:vagrant /home/vagrant/.kube/

echo "Provisioning vagrant specific kubeadm settings... complete!"