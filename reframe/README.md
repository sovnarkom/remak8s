# Reframework

Version: 1.19.1229.23

The Reframework is a set of simple libraries designed as a base for service oriented 
applications designed to work in a k8s cluster.

As a part of remak8s SDK Reframework will be used as an integrated working starting point for
distributed system development.

**But currently we only use it as a guinea pig for our immature CI/CD pipelines.**   

