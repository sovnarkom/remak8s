import asyncio
import logging
import os

from nats.aio.client import Client as NATS
from stan.aio.client import Client as STAN


class ReframeFunction:

    topic = None
    name = None

    def __init__(self, loop: asyncio.AbstractEventLoop):
        self.logger = logging.Logger(self.name)
        self.loop = loop
        self.sub = None
        self.sc = None
        self.nc = None

        # TODO: handle lists
        self.nats_servers = os.getenv('NATS_SERVERS')
        self.nats_streaming_cluster_id = os.getenv('NATS_STREAMING_CLUSTER_ID')

        self.logger.info("Function `%s` is initializing and subscribing to the `%s` topic", self.name, self.topic)

        self.loop.run_until_complete(self.subscribe())

        # TODO: we should not wait forever and instead handle graceful shutdown and max messages
        self.logger.info("Waiting for events...")
        self.loop.run_forever()

    async def handle(self, message):
        # TODO: Implement some sort of message handling
        self.logger.warning("Unhandled message %f", message)

    async def subscribe(self):

        self.logger.info("Connecting to the NATS cluster at: `%s`", self.nats_servers)

        self.nc = NATS()
        await self.nc.connect(self.nats_servers, io_loop=self.loop)

        self.logger.info(
            "Starting the NATS Streaming session: `%s` with the client id: `%s`",
            self.nats_streaming_cluster_id,
            self.name + str(id(self))  # TODO: better id
        )

        self.sc = STAN()
        await self.sc.connect(self.nats_streaming_cluster_id, self.name, nats=self.nc)

        self.logger.warning("Subscribing to events from the: `%s` topic", self.topic)
        # TODO: better queue name?
        self.sub = await self.sc.subscribe(self.topic, start_at='first', queue=self.name, cb=self.handle)

    async def finalize(self):
        # TODO: now this is never called, we should handle this gracefully instead
        await self.sub.unsubscribe()
        await self.sc.close()
        await self.nc.close()
