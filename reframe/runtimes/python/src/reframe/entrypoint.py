import asyncio
import os

import logging
from reframe.function import ReframeFunction

logging.basicConfig(level=logging.INFO)

# TODO: this will be available in the runtime container, but i think we can handle
#  this better (importlib?)
import handler


class EntrypointReframeFunction(ReframeFunction):

    name = os.getenv('FUNCTION_NAME')
    topic = os.getenv('FUNCTION_TOPIC')

    async def handle(self, message):
        self.logger.warning("got message  %s", message)
        return await handler.handler(message, runtime=self)


function = EntrypointReframeFunction(asyncio.get_event_loop())
function.subscribe()

