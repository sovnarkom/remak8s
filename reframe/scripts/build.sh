#!/usr/bin/env bash

echo "Provisioning reframe base CI experiments"

DIR=/remak8s/reframe/
NAPESPACE="default"

source /remak8s/var/gitlab-secrets

#buildah bud /remak8s/operator/ -t registry.remak8s.k8s/reframe:latest

NAME=reframe-example-service

kubectl delete taskrun --all

# example
gryml \
  -f $DIR/services/nats-producer/gryml/local.gryml.yml \
  --echo | kubectl apply -f -

gryml \
  -f $DIR/services/nats-consumer/gryml/local.gryml.yml \
  --echo | kubectl apply -f -

# example
#gryml \
#  -f $DIR/services/example/gryml/local.gryml.yml \
#  --echo | kubectl apply -f -


# This is the 'prod' config with the remote git source and remote image registry (To be completed)
#gryml \
#  -f $DIR/services/example/gryml/prod.gryml.yml \
#  --set git.auth.username="$GIT_USERNAME" \
#  --set git.auth.password="$GIT_PASSWORD" \
#  --set application.name=$NAME \
#  --echo \
#  | kubectl apply -f -

# This is the function artifact type deployment, we are not sure yet if we need it
#gryml $DIR/tekton/function \
#  -f $DIR/gryml/local.gryml.yml \
#  --echo \
#  | kubectl apply -f -

  #--set image.url="registry.remak8s.k8s/reframe:latest" \ # TODO: local registry with a proper domain

echo "Provisioning reframe base CI experiments... complete!"
