#!/usr/bin/env bash

clear

echo "Provisioning reframe base CI experiments (functions)"

DIR=/remak8s/reframe/
NAPESPACE="default"

source /remak8s/var/gitlab-secrets

kubectl delete taskrun --all
kubectl delete deployment/function-reframe-example-local --wait # TODO: hmm

gryml \
  -f $DIR/functions/reframe-example/gryml/local.gryml.yml \
  --echo | kubectl apply -n $NAPESPACE -f -

echo "Provisioning reframe base CI experiments (functions)... complete!"
