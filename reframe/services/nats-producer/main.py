import asyncio
from nats.aio.client import Client as NATS
from stan.aio.client import Client as STAN


async def run(loop):

    nc = NATS()
    print("connecting nats")
    await nc.connect("default.nats", io_loop=loop)

    # Start session with NATS Streaming cluster.
    print("connecting stan")
    sc = STAN()
    await sc.connect("default-stan", "client-producer", nats=nc)

    # Synchronous Publisher, does not return until an ack
    # has been received from NATS Streaming.
    print("sending", "hi: hello")
    await sc.publish("reframe-example-topic", b'hello')

    print("sending", "hi: world")
    await sc.publish("reframe-example-topic", b'world')

    # Close NATS Streaming session
    await sc.close()

    # We are using a NATS borrowed connection so we need to close manually.
    await nc.close()

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(loop))
    loop.close()