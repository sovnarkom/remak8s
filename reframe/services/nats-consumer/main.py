import asyncio
import os

from nats.aio.client import Client as NATS
from stan.aio.client import Client as STAN


async def run(loop):

    # Use borrowed connection for NATS then mount NATS Streaming
    # client on top.
    nc = NATS()
    await nc.connect(os.getenv('NATS_SERVERS'), io_loop=loop)

    # Start session with NATS Streaming cluster.
    sc = STAN()
    await sc.connect(os.getenv('NATS_STREAMING_CLUSTER_ID'), "client-consumer", nats=nc)

    total_messages = 0
    future = asyncio.Future(loop=loop)

    async def cb(msg):
        nonlocal future
        nonlocal total_messages
        print("Received a message (seq={}): {}".format(msg.seq, msg.data))
        total_messages += 1
        if total_messages >= int(os.getenv('MAX_EVENTS')):
            future.set_result(None)

    # Subscribe to get all messages since beginning.
    sub = await sc.subscribe("reframe-example-topic", start_at='first', cb=cb)
    await asyncio.wait_for(future, 1, loop=loop)

    # Stop receiving messages
    await sub.unsubscribe()

    # Close NATS Streaming session
    await sc.close()

    # We are using a NATS borrowed connection so we need to close manually.
    await nc.close()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(loop))
    loop.close()