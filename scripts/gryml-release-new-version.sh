#!/usr/bin/env bash

# This script should be executed from the remak8s SDK root

TITLE="Releasing new Gryml version to pip and git"

echo "$TITLE..."

python version.py gryml/VERSION.md --update

VERSION=$(python version.py gryml/VERSION.md)

git add gryml/VERSION.md
git commit -m "Gryml / New version $VERSION released"

echo "Pushing to remak8s origin"
git push origin

echo "Pushing to gryml github"
./scripts/subtree-push-gryml.sh

echo "Provisioning to PYPI..."
./gryml/scripts/pip-provision.sh

echo "$TITLE done ($VERSION)."