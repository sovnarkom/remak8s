import asyncio
import os
import shutil
import threading

import git
import kopf


@kopf.on.create('zalando.org', 'v1', 'kopfexamples')
def create_fn(spec, meta, status, **kwargs):
    print(f"And here we are! Creating: {spec}")


def kopf_thread():
    loop = asyncio.new_event_loop()
    tasks = loop.run_until_complete(kopf.spawn_tasks())
    loop.run_until_complete(asyncio.wait(tasks))


async def git_pooler():

    DIR_NAME = "temp"
    REMOTE_URL = "git@gitlab.com:sovnarkom/remak8s.git"

    if os.path.isdir(DIR_NAME):
        shutil.rmtree(DIR_NAME)

    os.mkdir(DIR_NAME)

    repo = git.Repo.init(DIR_NAME)
    origin = repo.create_remote('origin', REMOTE_URL)

    print("Fetching...")
    origin.fetch()

    print("Fetched")
    print(repo.active_branch.log())

    while True:
        origin.pull(origin.refs[0].remote_head)
        print(origin.refs[0])
        print(repo.active_branch.log())
        await asyncio.sleep(10000)
        origin.fetch()


def main():

    kt = threading.Thread(target=kopf_thread)
    kt.start()

    loop = asyncio.get_event_loop()
    loop.run_until_complete(git_pooler())

    kt.join()


if __name__ ==  '__main__':
    main()
