# Versioning schema

All Somebugs Group projects use the mix of semver and calver for versioning:

`<major>.<minor:yearmonth>.<patch:day>-<pre-release:branch.stamp>[+<meta>]` 

Where 
- `year` is defined as `YYYY` or numeric year 
- `month` is defined as `M` or numeric month number 1-12
- `day` is defined as `D` or numeric day number 1-31 
- `branch` is defined as alphanumeric, i.e.: `alpha`, `beta1`, `stable`, `omega`
- `stamp` is defined as `HHMM` - timestamp in UTC
- `meta` is optional and may contain additional data ignored in versioning, i.e. sha of the commit.

For example: `0.202001.13-alpha.0922` is the "alpha" version of the product released January the 13th 2020 @ 09:22 UTC

So, while we use semver semantic in general, we do not follow the rules for `minor` and `patch` versioning.
We MUST automatically increment them according to the current year and month rather than based on deprecation or fixes.

### Purpose 

In our experience it's very hard to follow "semver" rules for minor and, especially, patch versions, 
while major version is often stuck at 0.

Instead, we want major versions to represent the API changes properly, while fixing minor and patch
versions to current year, month and day. This is backwards compatible with the existing tooling, and also
clearly represents how "old" the version is.

Developers have to manually maintain only `major` and `branch` parameters whenever major changes are
applied or new branch is created. Minor version is directly derived from current year and month
and is automatically updated each month, while patch version is aligned with the day of the month.

Pre-release tags are also derived from current hour and minute in UTC. 